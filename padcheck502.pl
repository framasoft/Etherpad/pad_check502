#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Getopt::Long;
use Config::YAML;
use Mojo::UserAgent;

my $c = Config::YAML->new(config => '/etc/padcheck502.yml');

my $instance;
my ($help, $verbose) = (0, 0);
shift @ARGV if ($ARGV[0] eq '--');
GetOptions(
    'instance|i=s' => \$instance,
    'verbose|v'    => \$verbose,
    'help|h'       => \$help,
);

print_usage(0) if $help;
print_usage(1) unless ($instance && $c->{$instance});

my $delay   = $c->{$instance}->{delay};
print_usage(2) unless ($delay && $delay > 0);

my $url     = $c->{$instance}->{url};
print_usage(3) unless ($url);

my $service = $c->{$instance}->{service};
print_usage(4) unless ($service);

my $sleep   = $c->{$instance}->{sleep} || 180;
print_usage(5) unless ($sleep > 0);

my $contact = $c->{$instance}->{contact};

my ($counter, $timer, $ua) = (0, 0, Mojo::UserAgent->new());

$verbose    = $c->{$instance}->{verbose} if (!$verbose && $c->{$instance}->{verbose});

while (1) {
    my $tx  = $ua->get($url);
    my $res = $tx->result;
    if ($res->code == 502) {
        $counter++;

        say sprintf('[WARNING] %s is returning 502 (%d time(s) since %d seconds)', ($instance, $counter, $timer)) if $verbose;

        $timer += $sleep;

        if ($timer >= $delay) {
            `/usr/sbin/service $service restart`;

            `echo "$service restarted\nDelay: $delay\nCounter: $counter\nSleep: $sleep" | mail -s "[padcheck502] $service restarted" $contact` if $contact;

            say sprintf('[WARNING] %s restarted', $service) if $verbose;

            $counter = 0;
            $timer   = 0;
        }
    }
    sleep $sleep;
}

sub print_usage {
    my $reason = shift;
    if ($reason == 1) {
        print <<EOF;
You don't have specified any instance or the specified instance is not in the configuration file.

EOF
    } elsif ($reason == 2) {
        print <<EOF;
The delay is not configured for the specified instance or is less than zero.

EOF
    } elsif ($reason == 3) {
        print <<EOF;
The url is not configured for the specified instance.

EOF
    } elsif ($reason == 4) {
        print <<EOF;
The service is not configured for the specified instance.

EOF
    } elsif ($reason == 5) {
        print <<EOF;
The sleep configured for the specified instance is less than zero.

EOF
    }
    print <<EOF;
padcheck502 (c) Framasoft 2016 GPLv3

Watch an etherpad instance and restart it if it returns a 502 status for a configurable delay.

Usage: padcheck502 [--instance|-i <instance>] [--verbose|-v] [--help|-h]

Options:
    --instance|-i <instance>    name of the instance to check
    --verbose|-v                verbose output (by default, padcheck502 only prints errors)
    --help|h                    prints this help and exits

Available Etherpad instances:
EOF
    for my $i (keys %{$c}) {
        say sprintf '    - %s', $i;
    }
    exit 1;
}
