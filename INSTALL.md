# INSTALLATION

## Dependencies

### Carton

Carton is a Perl dependencies manager, it will get what you need, so don't bother for Perl modules dependencies (but you can read the file `cpanfile` if you want).

```shell
sudo cpan Carton
```

Some modules that Carton will install need to be compiled. So you will need some tools:

```shell
sudo apt-get install build-essential libssl-dev
```

## Installation

After installing Carton:

```shell
cd /opt
git clone https://framagit.org/framasoft/pad_check502.git padcheck502
cd padcheck502
carton install
sudo cp padcheck502.yml.template /etc/padcheck502.yml
sudo chmod 640 /etc/padcheck502.yml
sudo vi /etc/padcheck502.yml
```

### padcheck502

You can see the options of `padcheck502` with:

```shell
/opt/padcheck502/padcheck502 -h
```

If you want to start `padcheck502` as a systemd service:

```shell
sudo cp padcheck502@.service /etc/systemd/system/padcheck502@.service
sudo systemctl daemon-reload
# To start padcheck502 on instance foo:
sudo systemctl start padcheck502@foo.service
# To start at boot:
sudo systemctl enable padcheck502@foo.service
```
